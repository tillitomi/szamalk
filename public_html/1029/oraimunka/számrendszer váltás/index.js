var A="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

function convert1()
{
	r1=parseInt(document.urlap.alap1.value);
	r2=parseInt(document.urlap.alap2.value);
	S=document.urlap.szam1.value;
	document.urlap.szam2.value=dectostring(stringtodec(S, r1), r2);
}

function convert2()
{
	r1=parseInt(document.urlap.alap1.value);
	r2=parseInt(document.urlap.alap2.value);
	S=document.urlap.szam2.value;
	document.urlap.szam1.value=dectostring(stringtodec(S, r2), r1);
}

function stringtodec(s, r)
{
	nd=0;
	m=1;
	
	for(i=s.length-1; i>=0; i--)
	{
		nd=nd+m*A.indexOf(s.charAt(i));
		m=m*r;
	}
	
	return nd;
}

function dectostring(n, r)
{
	s="";
	while(n>0)
	{
		s=A.charAt(n%r)+s;
		n=(n-(n%r))/r;
	}
	
	return s;
}